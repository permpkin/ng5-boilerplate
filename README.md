# Angular 5 Starter Kit

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.1.

## Comes loaded with
basic folder structure
firebase service
global observable service

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Globals Service Usage

getting and setting variables
```typescript
import { Component, OnInit } from '@angular/core';
import { Globals } from '@global';

@Component({
    template: '<p>Component Contents</p>'
})

export class SomeComponent implements OnInit {

	constructor( private Globals:Globals ) {}

	ngOnInit(){

		// returns variable if exists, else returns false
		let SomeVariable = this.Globals.get('SomeVariable');

		// sets variable and triggers behavioursubject change ( sent to subscribers, see below )
		this.Globals.set('SomeVariable',{anytype:'ofdata'})

		// removes variable and destroys all subscriptions to that variable
		this.Globals.clear('SomeVariable')

		// returns true/false whether variable is set ( unless variable value is set as 'false' )
		this.Globals.has('SomeVariable')
		
		// clears all variables, subscriptions ( useful for functions such as logout functions )
		this.Globals.wipe()

	}

}
```

using single subscriptions
```typescript
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Globals } from '@global';

@Component({
    template: '<p>Component Contents</p>'
})

export class SomeComponent implements OnInit, OnDestroy {

	Subscriber:Subscription;

	constructor( private Globals:Globals ) {}

	ngOnInit(){

		this.Subscriber = this.Globals.observe('SomeGlobalParam')
			.subscribe((Data) => {

				//Do Something

			});

	}

	ngOnDestroy(){

		this.Subscriber.unsubscribe();

	}

}
```

using multiple subscriptions
```typescript
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Globals } from '@global';

@Component({
    template: '<p>Component Contents</p>'
})

export class SomeComponent implements OnInit, OnDestroy {

	constructor( private Globals:Globals ) {}

	ngOnInit(){

		this.Globals.subscribe(this.Globals.observe('SomeGlobalParam')
			.subscribe((Data) => {

				//Do Something

			});
		);

		this.Globals.subscribe(this.Globals.observe('SomeOtherParam')
			.subscribe((Data) => {

				//Do Something

			});
		);

	}

	ngOnDestroy(){

		this.Globals.unsubscribe();

	}

}
```