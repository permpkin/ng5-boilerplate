import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

@Injectable()
export class AccessGuard implements CanActivate {

    constructor() { }

    canActivate() {
        return true;
    }

}