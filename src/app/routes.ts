import { Routes, RouterModule } from '@angular/router';
import { Environment } from '@environment';

import { HomeView, AccountView, NotFound } from '@views';

import { AccessGuard } from '@guards';

const RouteMap: Routes = [

    { path: '', component: HomeView },

    { path: 'my-account', component: AccountView, canActivate: [AccessGuard] },

    { path: '**', component: NotFound, data: { sidebar: false } }

];

export const Routing = RouterModule.forRoot(RouteMap);