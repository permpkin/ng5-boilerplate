import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { Environment } from '@environment';
import { App } from '@app';
import { Routing } from '@routes';
import { Globals } from '@services';
import { AccessGuard } from '@guards';

import { NotFound, HomeView, AccountView } from '@views';

@NgModule({
  imports: [

    BrowserModule,
    Routing,
    FormsModule,
    ReactiveFormsModule

  ],
  declarations: [
    App,
    NotFound,
    HomeView,
    AccountView
  ],
  providers: [
    Globals,
    AccessGuard
  ],
  bootstrap: [
    App
  ]
})

export class AppModule {}
